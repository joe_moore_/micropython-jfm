
url = 'https://bitbucket.org/joe_moore_/micropython-jfm/get/master.tar.gz'

import urllib2.urequest as request
import gzip

tarball = request.urlopen(url)

def tar_getfile:
  return [ ("hello.txt", "Hello World!\n") ]

for (filename,data) in tar_getfile(gzip.uncompress(tarball))
  f = open(filename,"w")
  f.write(data)
  f.close()
