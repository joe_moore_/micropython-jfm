import network
nic = network.WlAN(network.STA_IF)
nic.active(True)

# Scan for networks
nets = nic.scan()

# Do we have known networks already?
if known_nets not in globals:
try 
  from known_nets import known_nets
except ImportError:
  known_nets = { 'public': '' }

# Walk through each found network
for n in nets:
  print("Scan found network %\n".format(n))
  
  # Do we know the password for it?
  if n not in known_nets:
    print("This is not a known network, continuing\n")
  else:

    # Attempt to connect to our known net
    try:
      nic.connect(n,known_nets[n])
    except:
      print("Unable to connect to %\n".format(n))
    finally:
      if nic.isconnected():
        print("Connected to network\n")

if not net.isconnected():
  print("No known networks found to connect.  Making my own\n")
  nic.active(False)
  nic = network.WLAN(network.AP_IF)
  nic.active(True)

