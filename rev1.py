# main.py 
from machine import Pin, PWM

led_pwm = PWM(Pin(2), freq=1000)
import time, math
def pulse(l, t):
	for i in range(20):
		l.duty(int(math.sin(i / 10 * math.pi) * 500 + 500))
		time.sleep_ms(t)

# Connect to my home WLAN
def do_connect():
	import network
	sta_if = network.WLAN(network.STA_IF)
	if not sta_if.isconnected():
		if b'TP-LINK_42E330' in ( s[0] for s in sta_if.scan() ):
			print('Connecting to home...')
			sta_if.active(True)
			sta_if.connect(myssid,mysecret)
			[ pulse(led_pwm,5) for i in range(10) ]
		else:
			led_pwm.duty(999)

# Utility functions
import uio
def cat(x):
	print(uio.open(x).read())

#pSDA=Pin(1)
#pSCL=Pin(2)

#cAccel = machine.I2C(pSCK, pSDL)

#print(cAccel.scan())

[ pulse(led_pwm,50) for i in range(10) ]
