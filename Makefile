#!/usr/bin/make -f
TARGETS=main.py known_nets.py micropython-jfm/__init.py micropython-jfm/hwconfig.py

all:

up:
	webrepl_cli.py main.py esp-d1:/main.py

wipe:
	esptool.py erase_flash
	esptool.py write_flash esp8266-20170612-v1.9.1.bin
